from neo4j import GraphDatabase as gd

#Credenciais 
'''
uri = "bolt://localhost:7687"
userName = "neo4j"
password = "123456"
'''

class bancoNeo4j():
    def __init__(self, uri, userName, password):
        self.__driver  = gd.driver(uri, auth=(userName, password)) 

    def query(self, cmd):
        with self.__driver.session() as session:
            nodes = session.run(cmd)
            if nodes != None:
                return nodes


bd = bancoNeo4j("bolt://localhost:7687", "neo4j", "123456")
nodes = bd.query("MATCH (n) RETURN n")
for node in nodes:
    print(node)

