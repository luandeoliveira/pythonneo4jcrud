from PyQt5 import uic 
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import sys 

class addProduto(QDialog):
    def __init__(self):
        super(addProduto, self).__init__()
        uic.loadUi('ui/addProduto.ui', self)        
        self.show()

class janelaPrincipal(QMainWindow):
    def __init__(self):
        super(janelaPrincipal, self).__init__()
        uic.loadUi('ui/principal.ui', self)
        self.cadastrarProduto.triggered.connect(lambda: self.abrirJanela(addProduto))       
        self.gerenciarProduto.triggered.connect(lambda: self.abrirJanela(gerenciarProduto))  
        self.show()

    def abrirJanela(self, janela):                                            
        self.j = janela()
        self.j.show()
            

class gerenciarProduto(QMainWindow):
    def __init__(self):
        super(gerenciarProduto, self).__init__()
        uic.loadUi('ui/produto.ui', self)        
        self.show()

class novaVenda(QMainWindow):
    def __init__(self):
        super(novaVenda, self).__init__()
        uic.loadUi('ui/venda.ui', self)        
        self.show()


if __name__ == "__main__":    
    app = QApplication(sys.argv)
    window = janelaPrincipal()
    sys.exit(app.exec())


